import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
	// MainLayout
	{
		path: "/",
		name: "MainLayout",
		component: () => import("@/layout/MainLayout"),
		redirect: { name: "MainMapStatistic" },
		children: [
			{
				path: "map-statistic",
				name: "MainMapStatistic",
				component: () => import("@/views/MapStatistic"),
			},
			{
				path: "map-statistic/:id",
				name: "MainMapStatisticShow",
				component: () => import("@/views/MapStatistic/show"),
			},
			{
				path: "statistics",
				name: "MainStatistics",
				component: () => import("@/views/Statistics"),
				redirect: { name: "CreatingNewJobs" },
				children: [
					{
						path: "creating-new-jobs",
						name: "CreatingNewJobs",
						component: () =>
							import("@/views/Statistics/CreatingNewJobs"),
					},
					{
						path: "register-new-entrepreneurs",
						name: "RegisterNewEntrepreneurs",
						component: () =>
							import(
								"@/views/Statistics/RegisterNewEntrepreneurs"
							),
					},
					{
						path: "register-self-employed",
						name: "RegisterSelfEmployed",
						component: () =>
							import("@/views/Statistics/RegisterSelfEmployed"),
					},
					{
						path: "persons-state-registration",
						name: "PersonsStateRegistration",
						component: () =>
							import(
								"@/views/Statistics/PersonsStateRegistration"
							),
					},
				],
			},
		],
	},

	// AuthLayout
	{
		path: "/auth",
		name: "AuthLayout",
		component: () => import("@/layout/AuthLayout"),
		redirect: { name: "AuthLogin" },
		children: [
			{
				path: "login",
				name: "AuthLogin",
				component: () => import("@/views/Auth/Login"),
			},
		],
	},
];

const router = new VueRouter({
	mode: "history",
	base: process.env.BASE_URL,
	routes,
});

export default router;
