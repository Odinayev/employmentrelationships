export default {
	props: {
		value: {
			type: [Number, String, Array, Object],
			default: () => "",
		},
	},
	computed: {
		content: {
			get() {
				return this.value;
			},
			set(value) {
				this.$emit("change", value);
			},
		},
	},
};
