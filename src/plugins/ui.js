import Vue from "vue";
import AppSelect from "@/common/components/AppSelect";
import AppButton from "@/common/components/AppButton";
import AppWrapper from "@/common/components/AppWrapper";
import AppTextField from "@/common/components/AppTextField";
import AppPagination from "@/common/components/AppPagination";

Vue.component("AppSelect", AppSelect);
Vue.component("AppButton", AppButton);
Vue.component("AppWrapper", AppWrapper);
Vue.component("AppTextField", AppTextField);
Vue.component("AppPagination", AppPagination);
